import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class logowanie {

    public static void main(String[] args) {
        String url = "http://automationpractice.com/index.php";
        String email = "stop6@onet.eu";
        String password = "spoko";

        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();

        driver.get(url);

        WebElement signInButton = driver.findElement(By.xpath("//*[@id=\"header\"]/div[2]/div/div/nav/div[1]/a"));
        signInButton.click();

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"email\"]")));

        WebElement emailLogin = driver.findElement(By.xpath("//*[@id=\"email\"]"));
        emailLogin.sendKeys(email);
        WebElement passwordLogin = driver.findElement(By.xpath("//*[@id=\"passwd\"]"));
        passwordLogin.sendKeys(password);

        WebElement signIn = driver.findElement(By.xpath("//*[@id=\"SubmitLogin\"]/span"));
        signIn.click();

        WebElement signOut = driver.findElement(By.xpath("//*[@id=\"header\"]/div[2]/div/div/nav/div[2]/a"));
        signOut.click();

        driver.quit();


}}
