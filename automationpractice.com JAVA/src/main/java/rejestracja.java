import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class rejestracja {

    public static void main(String[] args) {
        String url = "http://automationpractice.com/index.php";
        String email = "stop6@onet.eu";
        String first_name = "Lola";
        String last_name = "Koko";
        String password1 = "spoko";
        String adress1 = "Malinowa 12";
        String city1 = "Katowice";
        String zipcode = "40164";
        String phoneNumber = "123456789";
        String alias = "dom";

        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();

        driver.get(url);

        WebElement signInButton = driver.findElement(By.xpath("//*[@id=\"header\"]/div[2]/div/div/nav/div[1]/a"));
        signInButton.click();

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"email_create\"]")));

        WebElement emailCreate = driver.findElement(By.xpath("//*[@id=\"email_create\"]"));
        emailCreate.sendKeys(email);

        WebElement createAccountButton = driver.findElement(By.xpath("//*[@id=\"SubmitCreate\"]/span"));
        createAccountButton.click();

        WebDriverWait wait1 = new WebDriverWait(driver, 10);
        wait1.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"customer_firstname\"]")));

        WebElement firstName = driver.findElement(By.xpath("//*[@id=\"customer_firstname\"]"));
        firstName.sendKeys(first_name);

        WebElement lastName = driver.findElement(By.xpath("//*[@id=\"customer_lastname\"]"));
        lastName.sendKeys(last_name);

        WebElement password = driver.findElement(By.xpath("//*[@id=\"passwd\"]"));
        password.sendKeys(password1);

        WebElement firstNameAgain = driver.findElement(By.xpath("//*[@id=\"firstname\"]"));
        firstNameAgain.sendKeys(first_name);

        WebElement lastNameAgain = driver.findElement(By.xpath("//*[@id=\"lastname\"]"));
        lastNameAgain.sendKeys(last_name);

        WebElement adress = driver.findElement(By.xpath("//*[@id=\"address1\"]"));
        adress.sendKeys(adress1);

        WebElement city = driver.findElement(By.xpath("//*[@id=\"city\"]"));
        city.sendKeys(city1);

        WebElement element = driver.findElement(By.xpath("//*[@id=\"id_state\"]"));
        Select selectElement = new Select(element);
        selectElement.selectByVisibleText("Alabama");

        WebElement zipCode = driver.findElement(By.xpath("//*[@id=\"postcode\"]"));
        zipCode.sendKeys(zipcode);

        WebElement mobilePhoneNumber = driver.findElement(By.xpath("//*[@id=\"phone_mobile\"]"));
        mobilePhoneNumber.sendKeys(phoneNumber);

        WebElement aliasName = driver.findElement(By.xpath("//*[@id=\"alias\"]"));
        aliasName.sendKeys(alias);

        WebElement registerButton = driver.findElement(By.xpath("//*[@id=\"submitAccount\"]/span"));
        registerButton.click();

        WebElement signOut = driver.findElement(By.xpath("//*[@id=\"header\"]/div[2]/div/div/nav/div[2]/a"));
        signOut.click();

        driver.quit();


    }}
