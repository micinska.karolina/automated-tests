@registration

Feature: As a unauthorized user I want to register so I will be able to book a flight

  Background:
    Given user is on a newtours main page


  Scenario Outline: registration
    When he clicks register button
    And he fills the form as "<username>" with password "<password>"
    Then "<username>" should be registered properly
    Examples:
      | username  | password    |
      | Karolina  | tajnehaslo  |
      | Karolina1 | tajnehaslo1 |


  @rejestracja
  Scenario Outline: registration with optional data
    When he clicks register button
    And he fills additional data: "<firstname>", "<lastname>", "<phone>", "<email>"
    And he fills the form as "<username>" with password "<password>"
    Then "<username>" should be registered properly
    Examples:
      | username  | password    | firstname | lastname | phone     | email           |
      | Karolina  | tajnehaslo  | Karo      | Koko     | 123456789 | stop@gmail.com  |