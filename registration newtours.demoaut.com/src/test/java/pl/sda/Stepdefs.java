package pl.sda;

import cucumber.api.java.After;
import cucumber.api.java.AfterStep;
import cucumber.api.java.Before;
import cucumber.api.java.BeforeStep;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.Assert.*;

public class Stepdefs {
    WebDriver driver;


    @Before ()
    public void before() {

    }

    @After ()
    public void after() {
        driver.quit();

    }

//    @BeforeStep
//    public void beforeStep(){
//        System.out.println("before step");
//    }
//
//    @AfterStep
//    public void afterStep(){
//        System.out.println("after step");
//    }

    @Given("")
    public void step1() {
        
    }

    {

    }

    @Given("User is on a web page")
    public void userIsOnAWebPage() {
        
    }

    @When("He clicks log in")
    public void heClicksLogIn() {
        
    }

    @Then("He should be redirected to login page")
    public void heShouldBeRedirectedToLoginPage() {
    }

    @Given("user is on a newtours main page")
    public void userIsOnANewtoursMainPage() {

        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");

        driver = new ChromeDriver();

        driver.get("http://newtours.demoaut.com");

        driver.manage().window().maximize();
    }

    @When("he clicks register button")
    public void heClicksRegisterButton() {
        WebElement registerButton = driver.findElement(By.partialLinkText("REGISTER"));
        registerButton.click();

    }

    @And("he fills the form as {string} with password {string}")
    public void heFillsTheFormAsWithPassword(String arg0, String arg1) {
        WebElement UsernameField = driver.findElement(By.id("email"));
        UsernameField.sendKeys(arg0);

        WebElement passwordField = driver.findElement(By.name("password"));
        passwordField.sendKeys(arg1);

        WebElement confirmPasswordField = driver.findElement(By.name("confirmPassword"));
        confirmPasswordField.sendKeys(arg1);

        WebElement submitButton = driver.findElement(By.name("register"));
        submitButton.submit();

    }
    @Then("{string} should be registered properly")
    public void shouldBeRegisteredProperly(String arg0) {
        By registrationConfirmationMessageLocator = By.xpath(String.format("//b[contains(text(), \"Note: Your user name is %s.\")]", arg0));
        driver.findElement(registrationConfirmationMessageLocator);
    }

    @And("he fills additional data: {string}, {string}, {string}, {string}")
    public void heFillsAdditionalData(String arg0, String arg1, String arg2, String arg3) {
        WebElement firstNameField = driver.findElement(By.name("firstName"));
        firstNameField.sendKeys(arg0);

        WebElement lastNameField = driver.findElement(By.name("lastName"));
        lastNameField.sendKeys(arg1);

        WebElement phoneField = driver.findElement(By.name("phone"));
        phoneField.sendKeys(arg2);

        WebElement emailField = driver.findElement(By.id("userName"));
        emailField.sendKeys(arg3);
    }


    }
