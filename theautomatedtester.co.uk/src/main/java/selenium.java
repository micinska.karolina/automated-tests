import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class selenium {
    public static void main(String[] args) {
        String pageTitle = "";

        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        driver.get("http://book.theautomatedtester.co.uk/chapter1");

        pageTitle = driver.getTitle();
        System.out.println(pageTitle);

        WebElement radiobutton = driver.findElement(By.id("radiobutton"));
        radiobutton.click();

        WebElement element = driver.findElement(By.id("selecttype"));
        Select selectElement = new Select(element);
        selectElement.selectByVisibleText("Selenium Grid");

        WebElement checkBox = driver.findElement(By.name("selected(1234)"));
        checkBox.click();

        if (checkBox.isSelected()) {
            System.out.println("Checkbox jest zaznaczony");
        } else {
            System.out.println("Checkbox nie jest zaznaczony");
        }

        WebElement isButton = driver.findElement(By.id("verifybutton"));
        Boolean isButtonPresent = driver.findElements(By.id("verifybutton")).size() > 0;
        System.out.println(isButtonPresent);

        WebElement findText = driver.findElement(By.xpath("//*[contains(text(),'Assert that this text is on the page')]"));
        Boolean isTextPresent = driver.findElements(By.xpath("//*[contains(text(),'Assert that this text is on the page')]")).size() > 0;
        System.out.println(isTextPresent);


        driver.quit();



    }




}
