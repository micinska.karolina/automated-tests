import time
from selenium import webdriver


def get_title():
    driver.get(url)

    page_title = driver.title
    print(page_title)


def registration():
    driver.get(url)

    driver.find_element_by_xpath("//*[@id=\"header\"]/div[2]/div/div/nav/div[1]/a").click()

    time.sleep(5)

    driver.find_element_by_xpath("//*[@id=\"email_create\"]").send_keys(email)
    driver.find_element_by_xpath("//*[@id=\"SubmitCreate\"]/span").click()

    time.sleep(5)

    driver.find_element_by_xpath("//*[@id=\"customer_firstname\"]").send_keys(first_name)
    driver.find_element_by_xpath("//*[@id=\"customer_lastname\"]").send_keys(last_name)
    driver.find_element_by_xpath("//*[@id=\"passwd\"]").send_keys(password1)
    driver.find_element_by_xpath("//*[@id=\"firstname\"]").send_keys(first_name)
    driver.find_element_by_xpath("//*[@id=\"lastname\"]").send_keys(last_name)
    driver.find_element_by_xpath("//*[@id=\"address1\"]").send_keys(adress1)
    driver.find_element_by_xpath("//*[@id=\"city\"]").send_keys(city1)

    driver.find_element_by_xpath("//*[@id=\"id_state\"]/option[text()='Alabama']").click()

    driver.find_element_by_xpath("//*[@id=\"postcode\"]").send_keys(zipcode)
    driver.find_element_by_xpath("//*[@id=\"phone_mobile\"]").send_keys(phoneNumber)
    driver.find_element_by_xpath("//*[@id=\"alias\"]").send_keys(alias)

    driver.find_element_by_xpath("//*[@id=\"submitAccount\"]/span").click()

    driver.find_element_by_xpath("//*[@id=\"header\"]/div[2]/div/div/nav/div[2]/a").click()


def login():
    driver.get(url)
    driver.find_element_by_xpath("//*[@id=\"header\"]/div[2]/div/div/nav/div[1]/a").click()
    time.sleep(3)

    driver.find_element_by_xpath("//*[@id=\"email\"]").send_keys("stop9@ont.eu")
    driver.find_element_by_xpath("//*[@id=\"passwd\"]").send_keys("spoko")
    driver.find_element_by_xpath("//*[@id=\"SubmitLogin\"]/span").click()

    driver.find_element_by_xpath("//*[@id=\"header\"]/div[2]/div/div/nav/div[2]/a").click()

    time.sleep(5)




driver = webdriver.Chrome(r'C:\chromedriver.exe')

url = "http://automationpractice.com/index.php"
email = "stop11@onet.eu"
first_name = "Lola"
last_name = "Koko"
password1 = "spoko"
adress1 = "Malinowa 12"
city1 = "Katowice"
zipcode = "40164"
phoneNumber = "123456789"
alias = "dom"
driver.maximize_window()



get_title()
registration()
login()


driver.close()