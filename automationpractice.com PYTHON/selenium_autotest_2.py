import time

from selenium import webdriver


def get_title():
    driver.get(url)

    page_title = driver.title
    print(page_title)


def check_ad():
    driver.get(url)
    driver.find_element_by_xpath("//*[@id=\"htmlcontent_top\"]/ul/li[1]/a/img").click()

    time.sleep(5)


def add_to_cart():
    driver.get(url)

    driver.find_element_by_xpath("//*[@id=\"block_top_menu\"]/ul/li[1]/a").click()
    time.sleep(3)

    driver.find_element_by_xpath("//*[@id=\"center_column\"]/ul/li[1]/div/div[2]/h5/a").click()

    driver.find_element_by_xpath("//*[@id=\"quantity_wanted\"]").clear()

    driver.find_element_by_xpath("//*[@id=\"quantity_wanted\"]").send_keys(2)

    driver.find_element_by_xpath("//*[@id=\"group_1\"]/option[text()='M']").click()

    driver.find_element_by_xpath("//*[@id=\"add_to_cart\"]/button/span").click()

    time.sleep(3)

    driver.find_element_by_xpath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/a/span").click()

    driver.find_element_by_xpath("//*[@id=\"center_column\"]/p[2]/a[1]/span").click()




driver = webdriver.Chrome(r'C:\chromedriver.exe')

url = "http://automationpractice.com/index.php"
driver.maximize_window()


get_title()
check_ad()
add_to_cart()

driver.close()