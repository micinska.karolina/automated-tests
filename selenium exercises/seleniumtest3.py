import time
from datetime import datetime

from selenium import webdriver


driver = webdriver.Chrome(r'C:\chromedriver.exe')

url = "http://automationpractice.com/index.php"
driver.maximize_window()

driver.get(url)

driver.save_screenshot(f"stronaglowna {datetime.now().timestamp()}.png")

title = driver.title
print("tytul: " + title)

phoneNO = driver.find_element_by_xpath("/html/body/div/div[1]/header/div[2]/div/div/nav/span/strong").text
print("Phone no: " + phoneNO)

print("===============Rejestracja==============")
print()

driver.find_element_by_xpath("""/html/body/div/div[1]/header/div[2]/div/div/nav/div[1]/a""").click()

time.sleep(2)

driver.find_element_by_id("email_create").send_keys(f"{round(datetime.now().timestamp())}@zzrgg.com")
driver.find_element_by_xpath("/html/body/div/div[2]/div/div[3]/div/div/div[1]/form/div/div[3]/button/span").click()

time.sleep(5)

driver.find_element_by_id("id_gender1").click()
driver.find_element_by_id("customer_firstname").send_keys("Alex")
driver.find_element_by_id("customer_lastname").send_keys("Alex")
driver.find_element_by_id("passwd").send_keys("Alexi")

days = driver.find_element_by_id("days").find_elements_by_css_selector("*")
days[1].click()

months = driver.find_element_by_id("months").find_elements_by_css_selector("*")
months[1].click()

years = driver.find_element_by_id("years").find_elements_by_css_selector("*")
years[20].click()
# assert (years[10].text.strip() == "2011")
if years[10].text.strip() != "2011":
    raise ValueError("Year is not valid")

driver.find_element_by_id("firstname").send_keys("alex")
driver.find_element_by_id("lastname").send_keys("alex")
driver.find_element_by_id("company").send_keys("silesia kursy")
driver.find_element_by_id("address1").send_keys("mickiewicza 2")
driver.find_element_by_id("address2").send_keys("nie pamietam")
driver.find_element_by_id("city").send_keys("katowice")
driver.find_element_by_id("id_state").find_elements_by_css_selector("*")[2].click()
driver.find_element_by_id("postcode").send_keys("50100")
driver.find_element_by_id("phone_mobile").send_keys("124124124")
# driver.find_element_by_id("address_alias").send_keys("112412")
# driver.find_element_by_id("other").send_keys("test")
# driver.find_element_by_id("alias").clear()
# driver.find_element_by_id("alias").send_keys("123123")

time.sleep(2)
try:
    driver.find_element_by_id("submitAccount").click()
except:
    print("nie udalo sie nacisnac")
# driver.find_element_by_xpath("/html/body/div/div[2]/div/div[3]/div/div/form/div[4]/button/span").click()